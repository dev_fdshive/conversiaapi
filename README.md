# Using the Conversia API Library

## Requirements
* PHP 5.3.7 or newer
* cURL support

## Conversia API Setup
1. Init a new composer project:

    ```
    composer clearcache
    ```

    ```
    composer init
    ```

2. Configure repository:

    ```
    composer config repositories.fds vcs https://bitbucket.org/dev_fdshive/conversiaapi.git
    ```

3. Add dependency:

    ```
    composer require "fds/conversiaapi dev-master"
    ```

4. Enable API on Conversia at `https://converisa-url.test.com/s/config/edit` by checking "Enable HTTP basic auth?" and "API enabled?"


## Authorization
    
### Obtaining an access token
The first step is to obtain authorization.  Conversia supports OAuth 1.0a and OAuth 2 however it is up to the administrator
to decide which is enabled.  Thus it is best to have a configuration option within your project for the administrator 
to choose what method should be used by your code.

```php
<?php

// Bootup the Composer autoloader
include __DIR__ . '/vendor/autoload.php';  

use Conversia\Auth\ApiAuth;

session_start();

$publicKey = ''; 
$secretKey = ''; 
$callback  = ''; 

// ApiAuth->newAuth() will accept an array of Auth settings
$settings = array(
    'baseUrl'          => '',       // Base URL of the Conversia instance
    'version'          => 'OAuth2', // Version of the OAuth can be OAuth2 or OAuth1a. OAuth2 is the default value.
    'clientKey'        => '',       // Client/Consumer key from Conversia
    'clientSecret'     => '',       // Client/Consumer secret key from Conversia
    'callback'         => ''        // Redirect URI/Callback URI for this script
);

/*
// If you already have the access token, et al, pass them in as well to prevent the need for reauthorization
$settings['accessToken']        = $accessToken;
$settings['accessTokenSecret']  = $accessTokenSecret; //for OAuth1.0a
$settings['accessTokenExpires'] = $accessTokenExpires; //UNIX timestamp
$settings['refreshToken']       = $refreshToken;
*/

// Initiate the auth object
$initAuth = new ApiAuth();
$auth = $initAuth->newAuth($settings);

// Initiate process for obtaining an access token; this will redirect the user to the $authorizationUrl and/or
// set the access_tokens when the user is redirected back after granting authorization

// If the access token is expired, and a refresh token is set above, then a new access token will be requested

try {
    if ($auth->validateAccessToken()) {

        // Obtain the access token returned; call accessTokenUpdated() to catch if the token was updated via a
        // refresh token

        // $accessTokenData will have the following keys:
        // For OAuth1.0a: access_token, access_token_secret, expires
        // For OAuth2: access_token, expires, token_type, refresh_token

        if ($auth->accessTokenUpdated()) {
            $accessTokenData = $auth->getAccessTokenData();

            //store access token data however you want
        }
    }
} catch (Exception $e) {
    // Do Error handling
}
```

### Using Basic Authentication Instead
Instead of messing around with OAuth, you may simply elect to use BasicAuth instead.

Here is the BasicAuth version of the code above.

```php
<?php

// Bootup the Composer autoloader
include __DIR__ . '/vendor/autoload.php';  

use Conversia\Auth\ApiAuth;

session_start();

// ApiAuth->newAuth() will accept an array of Auth settings
$settings = array(
    'userName'   => '',             // Create a new user       
    'password'   => ''              // Make it a secure password
);

// Initiate the auth object specifying to use BasicAuth
$initAuth = new ApiAuth();
$auth = $initAuth->newAuth($settings, 'BasicAuth');

// Nothing else to do ... It's ready to use.
// Just pass the auth object to the API context you are creating.
```

**Note:** If the credentials are incorrect an error response will be returned.

```php
 array('error' => array(
       'code'    => 403,
       'message' => 'access_denied: OAuth2 authentication required' )
 )
 
```

## API Requests
Now that you have an access token and the auth object, you can make API requests.  The API is broken down into contexts.
Note that currently only the Contact context allows creating, editing and deleting items.  The others are read only.

### Get a context object

```php
<?php

use Conversia\ConversiaApi;

// Create an api context by passing in the desired context (Contacts, Forms, Pages, etc), the $auth object from above
// and the base URL to the Conversia server (i.e. http://my-Conversia-server.com/api/)

$api = new ConversiaApi();
$contactApi = $api->newApi('contacts', $auth, $apiUrl);
```

Supported contexts are currently:

See the [developer documentation](https://developer.Conversia.org).

### Retrieving items
All of the above contexts support the following functions for retrieving items:

```php
<?php

$response = $contactApi->get($id);
$contact = $response[$contactApi->itemName()];

// getList accepts optional parameters for filtering, limiting, and ordering
$response = $contactApi->getList($filter, $start, $limit, $orderBy, $orderByDir);
$totalContacts = $response['total'];
$contact = $response[$contactApi->listName()];
```

### Creating an item

```php
<?php

$fields = $contactApi->getFieldList();

$data = array();

foreach ($fields as $field) {
    $data[$field['alias']] = $_POST[$field['alias']];
}

// Set the IP address the contact originated from if it is different than that of the server making the request
$data['ipAddress'] = $ipAddress;
 
// Create the contact 
$response = $contactApi->create($data);
$contact = $response[$contactApi->itemName()];
```
    
### Editing an item
Currently, only Contacts support this

```php
<?php

$updatedData = array(
    'firstname' => 'Updated Name'
);

$response = $contactApi->edit($contactId, $updatedData);
$contact = $response[$contactApi->itemName()];

// If you want to create a new contact in the case that $contactId no longer exists
// $response will be populated with the new contact item
$response = $contactApi->edit($contactId, $updatedData, true);
$contact = $response[$contactApi->itemName()];
```
    
### Deleting an item

```php
<?php

$response = $contactApi->delete($contactId);
$contact = $response[$contactApi->itemName()];
```

### Error handling

```php
<?php

// $response returned by an API call should be checked for errors
$response = $contactApi->delete($contactId);

if (isset($response['error'])) {
    echo $response['error']['code'] . ": " . $response['error']['message'];
} else {
    // do whatever with the info
}
```